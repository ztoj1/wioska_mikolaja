#WIOSKA MIKOŁAJA

import pathlib
#path_log = pathlib.Path(r"E:\ZeroTOjunior\log.txt") # scieżka


class Habitant:
     counter_habitant=0
     def __init__(self, ID, name, surname, ID_building, date_birth, date_kill):
        self.ID=ID
        self.name=name
        self.surname=surname
        self.ID_building=ID_building
        self.date_birth=date_birth
        self.date_kill=date_kill
        Habitant.counter_habitant += 1       # odwołanie do pola statycznego
        self.counter_habitant = Habitant.counter_habitant     
        

     def __str__(self):
       return f"Dane elfa o ID {self.ID}: imię: {self.name}, nazwisko: {self.surname}, miejsce zamieszkania (ID budybku): {self.ID_building}, data urodzenia: {self.date_birth}, data dezaktywacji: {self.date_kill}. W wiosce mieszka {Habitant.counter_habitant} elfów"
     
     def create_habitant():
            ID = input ("wprowadź numer identyfikacyjny  elfa ID: ")        
            name = input ("Wprowadź imię: ")
            surname = input ("Wprowadź nazwisko: ")
            ID_building = input ("Wprowadź miejsce zamieszkania(ID budynku): ")
            date_birth = input ("Wprowadź datę urodzenia: ")
            date_kill=00-00-0000
            return Habitant(ID, name, surname, ID_building, date_birth, date_kill) 
            
     
     def speak_habitant  (self): 
            return f"Elf {self.name} mówi cześć" 

     def move_habitant (self):   #metoda służąca modyfikacji wartości pól
            new_ID_building=input ("Gdzie zamierzasz się przeprowadzić, wprowadz ID budynku: ")
            self.ID_building=new_ID_building 
            

     def kill_habitant (self):   #metoda służąca modyfikacji wartości pól
            new_date_kill=input ("Wprowadz datę dezaktywacji elfa: ")
            self.date_kill=new_date_kill                  
            Habitant.counter_habitant -= 1       # odwołanie do pola statycznego
            self.counter_habitant = Habitant.counter_habitant 
            

class Building:
     counter_building=0
     def __init__(self, ID, XYZ, name, volume, area, number_floor, ID_owner, date_start, date_stop):
             self.ID=ID
             self.XYZ=XYZ
             self.name=name
             self.volume=volume
             self.area=area
             self.number_floor=number_floor
             self.ID_owner=ID_owner
             self.date_start=date_start
             self.date_stop=00-00-0000
             Building.counter_building += 1       # odwołanie do pola statycznego
             self.counter_building = Building.counter_building
        
     def __str__(self):
             return f"Dane budynku o ID {self.ID}: XYZ: {self.XYZ}, nazwa: {self.name}, kubatura: {self.volume}, powierzchnia: {self.area}, ilosc pieter {self.number_floor}, wlasciciel (ID) {self.ID_owner}, data budowy: {self.date_start}, data zburzenia:{self.date_stop}. W wiosce jest {Building.counter_building} budynków/i"  
     
     def create_building():
             ID = input ("wprowadź numer identyfikacyjny budynku ID: ")        
             XYZ = input ("Wprowadź lokalizacje [X,Y,Z] ")
             name = input ("Wprowadź nazwę: ")
             volume = input ("Wprowadź kubaturę [m3]: ")
             area = input ("Wprowadź powierzchnię [m2]: ")
             name = input ("Wprowadź nazwę: ")
             number_floor= input ("wprowadź ilosc pieter: ")
             ID_owner = input ("Wprowadź własciciela (ID elfa): ")
             date_start = input ("Wprowadź datę wybudowania: ")
             date_stop = 00-00-0000
             return Building(ID, XYZ, name, volume, area, number_floor, ID_owner, date_start, date_stop) 

     def kill_building (self):   #metoda służąca modyfikacji wartości pól
             new_date_stop=input ("Wprowadz datę zburzenia budynku: ")
             self.date_stop=new_date_stop                  
             Building.counter_building -= 1       # odwołanie do pola statycznego
             self.counter_building = Building.counter_building   
       
class Building_factory(Building):
     counter_building_factory=0  
     def __init__(self, ID, XYZ, name, volume, area, number_floor, ID_owner, date_start, date_stop, ID_lines, ID_workers):  
            super().__init__(ID, XYZ, name, volume, area, number_floor, ID_owner, date_start, date_stop)
            self.ID_lines= ID_lines
            self.ID_workers = ID_workers
            Building_factory.counter_building_factory+=1
            self.counter_buildig_factory= Building_factory.counter_building_factory
            
            
     def __str__(self):
            return f"Dane fabryki o ID {self.ID}: XYZ: {self.XYZ}, nazwa: {self.name}, kubatura: {self.volume}, powierzchnia: {self.area}, ilosc pieter {self.number_floor}, wlasciciel (ID) {self.ID_owner}, data budowy: {self.date_start}, data zburzenia:{self.date_stop}. W wiosce jest {Building.counter_building} budynków/i. W wiosce jest {Building_factory.counter_building_factory} fabryk"
            
        
     def create_building_factory():
            #super().create_building() #nie działa to polecenie jak jest wprowadzanie z input / ???
            ID = input ("wprowadź numer identyfikacyjny budynku ID: ")        
            XYZ = input ("Wprowadź lokalizacje [X,Y,Z]: ")
            name = input ("Wprowadź nazwę: ")
            volume = input ("Wprowadź kubaturę [m3]: ")
            area = input ("Wprowadź powierzchnię [m2]: ")
            name = input ("Wprowadź nazwę: ")
            number_floor= input ("wprowadź ilosc pieter: ")
            ID_owner = input ("Wprowadź własciciela (ID elfa): ")
            date_start = input ("Wprowadź datę wybudowania: ")
            date_stop = 00-00-0000
            ID_lines = input ("Wprowadź identyfikator lini: ")
            ID_workers = input ("Wprowadź identyfikator pracowników")
            return Building_factory(ID, XYZ, name, volume, area, number_floor, ID_owner, date_start, date_stop, ID_lines, ID_workers)
            
     def kill_building_factory(self):
            super().kill_building()
            Building_factory.counter_building_factory -= 1       # odwołanie do pola statycznego
            self.counter_building_factory = Building_factory.counter_building_factory   
            return f"W wiosce  jest {Building.counter_building} budynków"            
        
     def change_lines(self):
             new_lines=input ("Wprowadz ID lini: ")
             self.lines=new_lines               
    
     def change_workers(self):
             new_ID_workers=input ("Wprowadz nową listę pracowników: ")
             self.ID_workers=new_ID_workers      
 
    
class Building_sledge(Building):
      counter_building_sledge=0  
      
      def __init__(self, ID, XYZ, name, volume, area, number_floor, ID_owner, date_start, date_stop, number_sledge):  
             super().__init__(ID, XYZ, name, volume, area, number_floor, ID_owner, date_start, date_stop)
             self.number_sledge= number_sledge
             Building_sledge.counter_building_sledge+=1
             self.counter_buildig_sledge= Building_sledge.counter_building_sledge
                
             
      def __str__(self):
             return f"Dane sankowni o ID {self.ID}: XYZ: {self.XYZ}, nazwa: {self.name}, kubatura: {self.volume}, powierzchnia: {self.area}, ilosc pieter {self.number_floor}, wlasciciel (ID) {self.ID_owner}, data budowy: {self.date_start}, data zburzenia:{self.date_stop}. W wiosce jest {Building_sledge.counter_building_sledge} sankowni"           
    
      def kill_building_sledge(self):
             super().kill_building()
             Building_sledge.counter_building_sledge -= 1       # odwołanie do pola statycznego
             self.counter_building_sledge = Building.counter_building_sledge   
             return f"W wiosce  jest {Building.counter_building} budynków i {Building_sledge.counter_building_sledge} sankowni"
    
      def send_order (self):
         if self.number_sledge>2:
            print("Jest wystarczająca iloc sani")
         else:
            print("wysyłam zamówienie do fabryki")
         
    
class Product:
    counter_product=0
    def __init__(self, ID, name, size, colour, ID_designer, date_start, date_stop, items ):
            self.ID=ID
            self.name=name
            self.size=size
            self.colour=colour
            self.ID_designer=ID_designer
            self.date_start=date_start
            self.date_stop=00-00-0000
            self.items=0
            Product.counter_product += 1       # odwołanie do pola statycznego
            self.counter_product = Product.counter_product

    def __str__(self):
           return f"{self.ID_designer} w {self.date_start} stworzył produkt o kodzie {self.ID} charaktreryzujący sie następującymi parametrami: {self.name}, {self.size}, {self.colour}. W wiosce jest produkowanych {Product.counter_product} różnych produktów"

      
    def kill_product(self, new_date_stop):     
        self.date_stop=new_date_stop                 
        Product.counter_product -= 1       # odwołanie do pola statycznego
        self.counter_product = Product.counter_product
        return f"Produkt {self.ID} został wycofany z produkcji {self.date_stop}"
        
    def add_items(self):
        new_items=input ("Wygeneruj parie produktu - wprowadź liczbę sztuk: ")
        self.items+=+new_items 
      
    def send_items(self, ID_building):
         return f"Produkt {self.ID} w iloci {self.item} został wysłany do budynku o ID {ID_building}"
     
class Animal:
    def __init__(self, ID, name, ID_building, date_birth):
        #założenie,że zwierzęta żyja wiecznie- brak date_kill
            self.ID=ID
            self.name=name
            self.ID_building=ID_building
            self.date_birth=date_birth
            
    def __str__(self): 
         return f"Zwierzę o imieniu {self.name} mieszka w budynku {self.ID_building} od {self.date_birth}"
    
class Reindeer(Animal):
    def __init__(self, ID, name, ID_building, date_birth, Id_sledge):
        super().__init__(ID, name, ID_building, date_birth)
        self.Id_sledge=Id_sledge
    
class Dog(Animal):
    def __init__(self, ID, name, ID_building, date_birth, ID_owner):
        super().__init__(ID, name, ID_building, date_birth)
        self.ID_owner=ID_owner


class Cat(Animal):
    def __init__(self, ID, name, ID_building, date_birth, ID_servant):
        super().__init__(ID, name, ID_building, date_birth)
        self.ID_servant=ID_servant
    
   
def main():
             habitant_1=Habitant.create_habitant()
             habitant_2=Habitant.create_habitant() #nie wiem dlaczego to dziala ??
             print(habitant_2)
             print(habitant_2.speak_habitant()) #print() -> wyświetla wynik funkcji  na ekranie
             habitant_2.move_habitant()
             print(habitant_2)
             habitant_2.kill_habitant()
             print(habitant_2)
             #with open(path_log, "a") as f:
                #f.write(str(habitant_2.__dict__))# nie wiem jak zrobic \n, dopisuje sie w lini
                  
             building_1=Building.create_building()  
             building_factory_1=Building_factory.create_building_factory()
             building_2=Building.create_building()  
             building_factory_2=Building_factory.create_building_factory()
             print(building_1)
             print(building_factory_1)
             print(building_2)
             print(building_factory_2)
             print(building_1.kill_building())
             print(building_factory_1.kill_building_factory())
             
             
             building_sledge_1=Building_sledge("S-1", "45-56-85", "Sankownia Elska Antusia", 4567, 234, 2, "H2-3", "23-05-1900", "00-00-0000", 4)
             building_sledge_1.send_order()
             print(building_sledge_1)
            
if __name__== "__main__":
		main()